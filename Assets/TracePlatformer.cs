﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TracePlatformer : MonoBehaviour
{
    public string HorizontalAxis;
    public string VerticalAxis;
    public float Aim;
    public float AimTime = 0.3f;
    public float Friction = 10;
    public float Acceleration = 1;
    public float MaximumHorizontalSpeed = 10;
    public float Gravity = 20;
    public float MaximumUpSpeed = 20;
    public float MaximumDownSpeed = 20;
    public float HorizontalAxisValue;
    public string JumpButton;
    public Vector2 Velocity;
    public Vector2 HitSize = Vector2.one;
    public LayerMask SolidMask;
    public LayerMask GroundMask;
    public UnityEvent OnJump, OnGround, OnAir;
    public bool IsGrounded;
    public bool IsSkidding;
    public float JumpSpeed = 10;
    // Use this for initialization
    void Start() {

    }
    public void ApplyVelocity(GameObject toInherit)
    {
        var vh = toInherit.GetComponent<IVelocityInherit>();
        if (vh != null) vh.Inherit(this.Velocity);
    }
    public void TeleportTo(Vector2 v)
    {
        this.transform.position = v;
        WallTest();
    }
    bool IgnoreDropDownPlatforms;
    IEnumerator DropDownPlatform()
    {
        Velocity.y = -1;
        TransitionToAir();
        IgnoreDropDownPlatforms = true;
        yield return new WaitForSeconds(0.3f);
        IgnoreDropDownPlatforms = false;
    }
    public void AirJump(float Speed)
    {
        Velocity.y = Speed;
        TransitionToAir();
    }
    public bool isJumping = false;
    // Update is called once per frame
    void Update ()
    {
        var aimTarget = Input.GetAxis(VerticalAxis);

        HorizontalAxisValue = Input.GetAxis(HorizontalAxis);
        IsSkidding = false;
        if (!this.IsGrounded)
        {
            Velocity.y -= Time.deltaTime * 20;
        }
        if (Input.GetButtonUp(JumpButton)) {
            if (!IsGrounded && isJumping  && Velocity.y > 0)
            {
                Velocity.y = 0;
            }
            isJumping = false;
        }
        if(IsGrounded && Input.GetButtonDown(JumpButton))
        {
            if (aimTarget < 0 && HorizontalAxisValue == 0)
            {
                StartCoroutine(DropDownPlatform());
            }
            else
            {
                isJumping = true;
                OnJump.Invoke();
                Velocity.y = JumpSpeed;
                TransitionToAir();
            }
        }
        Aim = Mathf.MoveTowards(Aim, aimTarget, Time.deltaTime / AimTime);
        if (HorizontalAxisValue == 0)
        {
            var sign = Mathf.Sign(Velocity.x);
            Velocity.x -= sign * Friction * Time.deltaTime;
            if (sign != Mathf.Sign(Velocity.x)) Velocity.x = 0;
        }
        else
        {
            if (Velocity.x == 0 || Mathf.Sign(HorizontalAxisValue) == Mathf.Sign(Velocity.x))
            {
                Velocity.x += HorizontalAxisValue * Time.deltaTime * Acceleration;
            } else
            { 
                    IsSkidding = true;
                Velocity.x = 0;// HorizontalAxisValue * Time.deltaTime * (Acceleration + Friction) * 10;
                 
            }
        }
        Velocity.y = Mathf.Clamp(Velocity.y, -MaximumUpSpeed, MaximumDownSpeed);
        Velocity.x = Mathf.Clamp(Velocity.x, -MaximumHorizontalSpeed, MaximumHorizontalSpeed);
    }
    void FixedUpdate()
    { 
        this.transform.Translate(Velocity * Time.fixedDeltaTime, Space.World);
        DownTest();
        UpTest();
        WallTest();
    }
    RaycastHit2D Cast(Vector2 direction, float Size, LayerMask mask, Vector3? offset = null)
    {
        Debug.DrawLine(this.transform.position + offset.GetValueOrDefault(Vector3.zero), this.transform.position + offset.GetValueOrDefault(Vector3.zero) + (Vector3)direction * Size, Color.cyan, 1);
        return Physics2D.Raycast(this.transform.position + offset.GetValueOrDefault(Vector3.zero), direction, Size, mask);
    }
    void TransitionToAir()
    {
        IsGrounded = false;
        this.transform.SetParent(null);
        OnAir.Invoke();
    }
    void TransitionToGround()
    {
        IsGrounded = true;
        OnGround.Invoke();
    }
    void DownTest() {
        if (Velocity.y > 0) return;
        float offset = 0;
        var downHit = Cast(Vector2.down, HitSize.y * 1.1f, IgnoreDropDownPlatforms ? SolidMask : GroundMask);
        if (downHit.collider == null)
        {
            offset = HitSize.x * 0.5f;
            downHit = Cast(Vector2.down, HitSize.y * 1.1f, IgnoreDropDownPlatforms ? SolidMask : GroundMask, Vector2.right * offset);
        }
        if (downHit.collider == null)
        {
            offset = HitSize.x * -0.5f;
            downHit = Cast(Vector2.down, HitSize.y * 1.1f, IgnoreDropDownPlatforms ? SolidMask : GroundMask, Vector2.right * offset);
        }
        if (downHit.collider != null)
        {
            if (!IsGrounded) TransitionToGround();
            if(downHit.collider.tag == "MoveablePlatform")
            {
                this.transform.SetParent(downHit.collider.transform);
            } else
            {
                this.transform.SetParent(null);
            }
            this.Velocity.y = 0;
            Debug.DrawLine(this.transform.position,  (downHit.point - Vector2.down * HitSize.y), Color.red, 6);
            Debug.DrawLine(this.transform.position,  (downHit.point - Vector2.down * HitSize.y + (Vector2.right * offset)), Color.green, 6);
            this.transform.position = downHit.point - Vector2.down * HitSize.y - (Vector2.right * offset);
        } else
        {
            if (IsGrounded) TransitionToAir();
        }
    }
    void UpTest() {
        var upHit = Cast(Vector2.up, HitSize.y, SolidMask);
        if(upHit.collider != null)
        {
            this.Velocity.y = 0;
            this.transform.position = upHit.point + Vector2.down * HitSize.y;
        }
    }
    void WallTest()
    {
        int direction = (int)Mathf.Sign(Velocity.x);
        if (direction == 0) direction = 1;
        float offset = 0;
        var rightHit = Cast(Vector2.right * direction, HitSize.x, SolidMask);
        if (rightHit.collider == null)
        {
            offset = HitSize.y * 0.25f;
            rightHit = Cast(Vector2.right * direction, HitSize.x, SolidMask, Vector2.down * offset);
        }
        if (rightHit.collider == null)
        {
            offset = HitSize.y * -0.5f;
            rightHit = Cast(Vector2.right * direction, HitSize.x, SolidMask, Vector2.down * offset);
        }
        if (rightHit.collider != null)
        {
            this.Velocity.x = 0;
            this.transform.position = rightHit.point + Vector2.left * HitSize.x * direction - (Vector2.down * offset);
        }
        offset = 0;
        var leftHit = Cast(Vector2.left * direction, HitSize.x, SolidMask);
        if (leftHit.collider == null)
        {
            offset = HitSize.y * 0.25f;
            leftHit = Cast(Vector2.left * direction, HitSize.x, SolidMask, Vector2.down * offset);
        }
        if (leftHit.collider == null)
        {
            offset = HitSize.y * -0.5f;
            leftHit = Cast(Vector2.left * direction, HitSize.x, SolidMask, Vector2.down * offset);
        }
        if (leftHit.collider != null)
        {
            this.Velocity.x = 0;
            this.transform.position = leftHit.point - Vector2.left * HitSize.x * direction - (Vector2.down * offset);
        }

    }
    void OnEnable()
    {
        Velocity = Vector2.zero;
    }
    void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(this.transform.position, HitSize * 2);
    }
}
