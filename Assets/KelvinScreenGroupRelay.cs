﻿using com.clydebuildgames.common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KelvinScreenGroupRelay : MonoBehaviour {
    public StringEvent OnTextChange; 
    public KelvinCameraFollow KelvinFollower;
    public string LastSceneName = "";
    void Start()
    {
        if (KelvinFollower == null) this.GetComponent<KelvinCameraFollow>();
    }
    public void SetText(string Name)
    {
        if (LastSceneName == Name) return;
        LastSceneName = Name;
        OnTextChange.Invoke(Name);
    }
    public void RelayScreenChange()
    {
        var currentScreen = KelvinFollower.GetCurrentScreen();
        if(currentScreen.Group != null)
        {
            SetText(currentScreen.Group.AreaName);
        } else
        {
            SetText(currentScreen.name);
        }
    }
}
