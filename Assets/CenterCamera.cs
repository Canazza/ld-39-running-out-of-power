﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CenterCamera : MonoBehaviour {
    public float TargetRatio;
    public Camera TargetCamera; 
    public Vector2 currentResolution;
    void Awake()
    {
        if (TargetCamera == null) TargetCamera = this.GetComponent<Camera>();
        Set();
    }
    void Update()
    {
        if (ResolutionChanged) Set();
    }
    bool ResolutionChanged
    {
        get
        {
            return (currentResolution.x != ScreenWidth) || (currentResolution.y != ScreenHeight);
        }
    }
    int ScreenWidth
    {
        get
        {
            return Screen.width;
        }
    }
    int ScreenHeight
    {
        get
        {
            return Screen.height;
        }
    }
    void Set()
    {  
        currentResolution.x = ScreenWidth;
        currentResolution.y = ScreenHeight;
        if (ScreenHeight * TargetRatio < ScreenWidth)
        {
            TargetCamera.pixelRect = new Rect((ScreenWidth - (ScreenHeight * TargetRatio)) / 2, 0, ScreenHeight * TargetRatio, ScreenHeight);
        } else
        {
            TargetCamera.pixelRect = new Rect(0, (ScreenHeight - (ScreenWidth / TargetRatio)) / 2, ScreenWidth, ScreenWidth / TargetRatio);

        }
        
        TargetCamera.aspect = TargetRatio;
    }
}
