﻿using com.clydebuildgames.common;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class PropertyBinding : MonoBehaviour {
    public GameObject BindTo;
    public string ComponentName;
    public string VariableName;
    public string Format = "{0}";
    public enum VariableType
    {
        Property, Field
    }
    public VariableType Type = PropertyBinding.VariableType.Field;
    public StringEvent OnChange;
    public FloatEvent OnChangeNumber;
    public float min01, max01;
    public FloatEvent OnChange01;
    [Header("Filled in at runtime")]
    public Component component;
    public FieldInfo fieldInfo;
    public PropertyInfo propertyInfo;
    public bool IsFloat,IsInt;
    // Use this for initialization
    void Start () {
        component = BindTo.GetComponent(ComponentName);
        switch(Type)
        {
            case VariableType.Field:
                fieldInfo = component.GetType().GetField(VariableName);
                IsInt = fieldInfo.FieldType == typeof(int);
                IsFloat = fieldInfo.FieldType == typeof(float);
                break;
            case VariableType.Property:
                propertyInfo = component.GetType().GetProperty(VariableName);
                IsInt = propertyInfo.PropertyType == typeof(int);
                IsFloat = propertyInfo.PropertyType == typeof(float);

                break;
        }
	}
    public object previousValue;
    public object currentValue;
    public object GetCurrentValue()
    {
        return (fieldInfo != null) ? fieldInfo.GetValue(component) : (propertyInfo != null) ? propertyInfo.GetValue(component, null) : null;
       
    }
	// Update is called once per frame
	void Update () {
        currentValue = GetCurrentValue();
        if(previousValue != currentValue)
        {
            OnChange.Invoke(string.Format(Format,currentValue));
            if (IsFloat)
            {
                float v = (float)currentValue;
                OnChangeNumber.Invoke(v);
                OnChange01.Invoke((v / (max01 - min01)) + min01);
            }
            if (IsInt)
            {
                float v = (int)currentValue;
                OnChangeNumber.Invoke(v);
                OnChange01.Invoke((v / (max01 - min01)) + min01);
            }
            previousValue = currentValue;
        }
	}
}
