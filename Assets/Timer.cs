﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour {
    public UnityEvent OnTimerStart, OnTimerEnd;
    public float TimeInSeconds = 2;
    public bool StartOnEnable = true;
    public void StartTimer()
    {

        OnTimerStart.Invoke();
        Invoke("TimerEnd", TimeInSeconds);
    }
    void OnEnable()
    {
        if (StartOnEnable)
        {
            StartTimer();
        }
    }
    void TimerEnd()
    {
        OnTimerEnd.Invoke();
    }
}
