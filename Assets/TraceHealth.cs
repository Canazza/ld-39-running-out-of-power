﻿using com.clydebuildgames.common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TraceHealth : MonoBehaviour {
    public string ResetButton = "Fire1";
    public int HP = 150;
    public int CurrentHP = 150;
    public float MercyInvulnTime = 0.1f;
    public Vector2Event OnRespawn;
    public Vector2 RespawnPoint;
    public Rect Hitbox;
    public LayerMask HitLayers;
    public UnityEvent OnDeathStart;
    public float DeathTime;
    public UnityEvent OnDeathEnd;
    public UnityEvent OnRespawnStart;
    public float RespawnTime;
    public UnityEvent OnRespawnEnd;
    public UnityEvent OnHit;
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(this.transform.position + (Vector3)Hitbox.position, Hitbox.size);
    }
    public void ResetHP()
    {
        CurrentHP = HP;
    }
    public void Respawn()
    {
        CurrentHP = HP;
        OnRespawn.Invoke(RespawnPoint);
    }
    public void SetRespawnPoint(Vector3 point)
    {
        RespawnPoint = point;
    }
	// Use this for initialization
	void Start () {
        RespawnPoint = this.transform.position;
	}
	public void DealDamage(int damage)
    {
        if (IsInvuln) return;
        if (this.enabled == false) return;
        if (isDying) return;
        this.CurrentHP -= damage;
        if (this.CurrentHP <= 0) Kill();
        else
        {
            StartCoroutine(MercyInvuln());
            OnHit.Invoke();
        }
    }
    public bool IsInvuln;
    IEnumerator MercyInvuln()
    {
        IsInvuln = true;
        yield return new WaitForSeconds(MercyInvulnTime);
        IsInvuln = false;
    }
    public bool isDying;
    IEnumerator Death()
    { 
        isDying = true;
        OnDeathStart.Invoke();
        yield return new WaitForSeconds(DeathTime);
        OnDeathEnd.Invoke();
        OnRespawn.Invoke(RespawnPoint);
        OnRespawnStart.Invoke();
        yield return new WaitForSeconds(RespawnTime);
        OnRespawnEnd.Invoke();
        isDying = false; 
        Respawn();
    }
    void Kill()
    {
        if (!isDying) StartCoroutine(Death());
    }
	// Update is called once per frame
    public void TakeDamageFromSource(IDamageSource damageSource)
    {
        if (damageSource != null)
        {
            if (damageSource.IsInstantDeath)
            {
                Kill();
            }
            else
            {
                DealDamage(damageSource.GetDamage());
            }
            damageSource.DamageDelt();
        }

    }
	void Update () {
        var hit = Physics2D.OverlapBox((Vector2)this.transform.position + Hitbox.position, Hitbox.size, 0, HitLayers);
        if (hit != null)
        {
            TakeDamageFromSource(hit.GetComponent<IDamageSource>());           
        }
        if(!isDying && Input.GetButtonDown(ResetButton))
        {
            Kill();
        }
	}
}
public interface IDamageSource
{
    int GetDamage();
    void DamageDelt();
    bool IsInstantDeath { get; }
}