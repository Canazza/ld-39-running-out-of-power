﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CyclicMover : MonoBehaviour, IResettable {
    public float CycleTime;
    public Vector3 Distance;
    public float t;
    public float initialT;
    public Vector3 StartPosition;
    public AnimationCurve Curve;
    public bool StartDisabled = false;
	void Start()
    {
        StartPosition = this.transform.localPosition;
        if (StartDisabled) this.enabled = false;
    }
    void OnDrawGizmos()
    {
        if(!Application.isPlaying) 
            Gizmos.DrawLine(this.transform.position, this.transform.position + Distance);
    }
	// Update is called once per frame
	void Update () {
        t += Time.deltaTime / CycleTime;
        t %= Curve.keys[Curve.keys.Length - 1].time;
        this.transform.localPosition = Vector3.Lerp(StartPosition, StartPosition + Distance, Curve.Evaluate(t));
	}

    public void DoReset()
    {
        t = initialT;   
        if (StartDisabled) this.enabled = false;
        this.transform.localPosition = Vector3.Lerp(StartPosition, StartPosition + Distance, Curve.Evaluate(t));
    }
}
