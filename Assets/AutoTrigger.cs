﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoTrigger : MonoBehaviour {
    public LayerMask TriggerableLayers;
    public float TriggerRadius;
    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(this.transform.position, TriggerRadius);
    }
	
	// Update is called once per frame
	void Update () {
        var overlapped = Physics2D.OverlapCircleAll(this.transform.position, TriggerRadius);
        if(overlapped != null && overlapped.Length > 0)
        {
            foreach(var item in overlapped)
            {
                var triggerable = item.GetComponent<ITriggerable>();
                if (triggerable != null) triggerable.Trigger(this.gameObject);
            }
        }
	}
}
