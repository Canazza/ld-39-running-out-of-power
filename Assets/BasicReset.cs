﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BasicReset : MonoBehaviour, IResettable {
    public UnityEvent OnReset;
    public void DoReset()
    {
        OnReset.Invoke();
    }
}
