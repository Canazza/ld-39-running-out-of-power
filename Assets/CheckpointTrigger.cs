﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CheckpointTrigger : MonoBehaviour, ITriggerable {
    public static CheckpointTrigger CurrentCheckpoint;
    
    public UnityEvent OnTrigger,OnCurrent,OnNotCurrent;
    private bool isCurrent;
    public bool IsCurrent
    {
        get
        {
            return isCurrent;
        }
        set
        {
            if (isCurrent != value)
            {
                isCurrent = value;

                if (IsCurrent)
                {
                    OnCurrent.Invoke();
                }
                else
                {
                    OnNotCurrent.Invoke();
                }
            }
        }
    }
    public void Trigger(GameObject source)
    {
        if (CurrentCheckpoint == this) return;
        if (CurrentCheckpoint != null) CurrentCheckpoint.IsCurrent = false;
        CurrentCheckpoint = this;
        this.IsCurrent = true;
        var ability = source.GetComponent<TraceAbility>();
        if (ability != null) ability.ForceReloadFinish();
        OnTrigger.Invoke();
        source.GetComponent<TraceHealth>().RespawnPoint = this.transform.position;
    }

    void OnEnable()
    {
        if (IsCurrent)
        {
            OnCurrent.Invoke();
        }
        else
        {
            OnNotCurrent.Invoke();
        }
    } 
}
