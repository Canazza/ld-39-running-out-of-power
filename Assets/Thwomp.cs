﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thwomp : MonoBehaviour, IResettable {
    public float Distance = 4;
    public float WaitTime = 1;
    public float Speed = 10;
    public float ReturnSpeed = 2;
    public bool Automatic = false;
    public void OnDrawGizmos()
    { 
        Gizmos.DrawLine(this.transform.position, this.transform.position+ -this.transform.right * Distance);
    }
    public void DoReset()
    {
        isAttacking = false;
        StopAllCoroutines();
        this.transform.localPosition = StartPos; 
    } 
    public bool isAttacking;
    public void Attack()
    {
        if (isAttacking) return;
        StartCoroutine(DoAttack());
    }

    Vector3 StartPos;
    // Use this for initialization
    void Start () {
        StartPos = this.transform.localPosition;
	}
	IEnumerator DoAttack()
    {
        isAttacking = true;
        for(float t = 0; t < 1; t += Time.deltaTime * Speed/ Distance * 3)
        {
            this.transform.localPosition = Vector2.Lerp(StartPos, StartPos + this.transform.right * -Distance, t);
            yield return null;
        }
        this.transform.localPosition = StartPos + this.transform.right * -Distance;
        yield return new WaitForSeconds(WaitTime);
        for (float t = 0; t < 1; t += Time.deltaTime * ReturnSpeed / Distance * 3)
        {
            this.transform.localPosition = Vector2.Lerp(StartPos, StartPos + this.transform.right * -Distance, 1-t);
            yield return null;
        }
        this.transform.localPosition = StartPos;
        isAttacking = false;
    }
}
