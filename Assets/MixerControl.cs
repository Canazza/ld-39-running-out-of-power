﻿using com.clydebuildgames.common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MixerControl : MonoBehaviour {
    public UnityEngine.Audio.AudioMixer Mixer;
    public string FloatName = "MasterVolume";
    public FloatEvent OnValueSet;
    public AnimationCurve TransformationCurve; 

    // Use this for initialization
    void Start ()
    {
        SetValue(PlayerPrefs.GetFloat(FloatName, 0.5f));
	}
    public void SetValue(float t)
    {
        float dbValue = TransformationCurve.Evaluate(t);
        OnValueSet.Invoke(t);
        PlayerPrefs.SetFloat(FloatName, t);
        Mixer.SetFloat(FloatName, dbValue);
    }
	

}
