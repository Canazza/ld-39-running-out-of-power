﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraceAnimator : MonoBehaviour { 
    public Animator animator;
    public string Speed = "Speed";
    public string Grounded = "Grounded";
    public string Aim = "Aim";
    public TracePlatformer platformer;
    public void SetBool(string boolName)
    {
        animator.SetBool(boolName, true);
    }
    public void UnsetBool(string boolName)
    {
        animator.SetBool(boolName, false);
    }
    void Start()
    {
        platformer = platformer ?? this.GetComponent<TracePlatformer>();
        Flip = Vector3.one;
    }
    Vector3 Flip = new Vector3();
	// Update is called once per frame
	void Update ()
    {
        if (animator.gameObject.activeInHierarchy)
        {
            animator.SetFloat(Speed, Mathf.Abs(platformer.Velocity.x) / platformer.MaximumHorizontalSpeed);
            animator.SetFloat(Aim, platformer.Aim);
            animator.SetBool(Grounded, platformer.IsGrounded);
            Flip.x = platformer.Velocity.x > 0 ? -1 : platformer.Velocity.x < 0 ? 1 : Flip.x;
            animator.transform.localScale = Flip;
        }
	}
}
