﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ReloadTrigger : MonoBehaviour, ITriggerable, IResettable {
    public UnityEvent OnTrigger,OnReset;
    public UnityEvent OnDrained;
    public int Ammo = 100;
    public int AmmoRemaining;
    public void Start() {
        AmmoRemaining = Ammo;
    }

    public int ReloadByPerTick = 10;
    public float RepeatSpeed = 0.1f;
    public float ReloadTime = 0;
    public void DoReset()
    {
        AmmoRemaining = Ammo;
        ReloadTime = 0;
        OnReset.Invoke();
    }

    public void Trigger(GameObject source)
    {
        if (ReloadTime > 0) ReloadTime -= Time.deltaTime;
        var ability = source.GetComponent<TraceAbility>();
        if(ability != null)
        {
            if (ReloadByPerTick == 0)
            {
                ability.ForceReloadFinish();
                OnTrigger.Invoke();
            }
            else
            {
                if (ReloadTime <= 0 && AmmoRemaining > 0 && ability.AmmoLeft < ability.Ammo)
                {
                    var toAdd = Mathf.Min(ability.Ammo - ability.AmmoLeft, ReloadByPerTick, AmmoRemaining);
                    ability.AmmoLeft += toAdd;
                    AmmoRemaining -= toAdd;
                    ReloadTime = RepeatSpeed;
                    if (AmmoRemaining <= 0)
                        OnDrained.Invoke();
                    else
                        OnTrigger.Invoke();
                }
            }
        }

    }
     
	
	// Update is called once per frame
	void Update () {
		
	}
}
