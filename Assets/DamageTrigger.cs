﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DamageTrigger : MonoBehaviour, IDamageSource { 
    public int Damage = 1;
    public bool isInstantDeath;
    public UnityEvent OnDamageDealt;
    public bool IsInstantDeath
    {
        get
        {
            return isInstantDeath;
        }
    }

    public void DamageDelt()
    {
        OnDamageDealt.Invoke();
    }

    public int GetDamage()
    {
        return Damage;
    }
}
