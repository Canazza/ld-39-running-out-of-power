﻿using com.clydebuildgames.common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Spawner : MonoBehaviour {
    public GameObject Prefab;
    public List<GameObject> Pool;
    public GameObjectEvent OnSpawn;
    public bool ReverseDirection = false;
    public float Spread = 0;
    GameObject GetGameObject()
    {
        var existing = Pool.FirstOrDefault(x => !x.activeSelf);
        if(existing != null)
        {
            existing.SetActive(true);
            return existing;
        }
        var spawnNew = (GameObject)Instantiate(Prefab);
        Pool.Add(spawnNew);
        return spawnNew;
    }
    public void Spawn()
    {
        var toSpawn = GetGameObject();
        toSpawn.transform.position = this.transform.position;
        toSpawn.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles + GetRotationOffsetVector3())            ;
        OnSpawn.Invoke(toSpawn);
    }
    private float GetRandomSpread()
    {
        if (Spread <= 0) return 0;
        return UnityEngine.Random.Range(-Spread, Spread);
    }
    private Vector3 GetRotationOffsetVector3()
    {
        if (ReverseDirection)
        {
            return (this.transform.lossyScale.x < 0) ? new Vector3(0, 0, 180 + GetRandomSpread()) : new Vector3(0, 0,  GetRandomSpread());
        }
        else
        {
           return (this.transform.lossyScale.x > 0) ? new Vector3(0, 0, 180 + GetRandomSpread()) : new Vector3(0, 0,  GetRandomSpread());
        }
    } 

}
