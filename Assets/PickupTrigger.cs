﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PickupTrigger : MonoBehaviour,ITriggerable {
    public string ItemName;
    public UnityEvent OnTrigger;
    public void Trigger(GameObject source)
    {
        source.GetComponent<PlayerInventory>().AddToInventory(ItemName);
        OnTrigger.Invoke();
    } 
}
