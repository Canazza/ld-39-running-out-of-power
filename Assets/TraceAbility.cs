﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TraceAbility : MonoBehaviour {
    public int Ammo = 20;
    
    public float ReloadTime;
    public float Cooldown;
    public UnityEvent OnTrigger,OnReload,OnReloaded,OnPress, OnRelease;
    public string ButtonName;
    public string ReloadButtonName; 

    public float timeLeft = 0;
    public int AmmoLeft = 0;
    public float AutoReplenishEvery = 0;
    public bool IsReloading;
    public void Replenish(int n)
    {
        AmmoLeft += n;
        AmmoLeft = Mathf.Clamp(AmmoLeft, 0, Ammo);
    }
    public bool ReloadOnEnable = false;
    void Start() { AmmoLeft = Ammo; }
    void OnEnable()
    {
        timeLeft = 0;
        if (ReloadOnEnable)
        {
            AmmoLeft = Ammo;
        }
        if (AutoReplenishEvery > 0) StartCoroutine(Replenisher());
    }
    public float ReplenishProgress;
    
    public IEnumerator Replenisher()
    {
        
        while (true)
        {
            ReplenishProgress = 0;
            while (AmmoLeft < Ammo)
            {
                for (ReplenishProgress = 0; ReplenishProgress < 1; ReplenishProgress += Time.deltaTime / AutoReplenishEvery)
                {
                    yield return null;
                }
                Replenish(1);
            }
            yield return null;
        }

    }
    public void ForceReloadFinish()
    {
        timeLeft = 0;
        AmmoLeft = Ammo;
        IsReloading = false;
        OnReloaded.Invoke();
    }
    public bool IsDown = false;
    private bool downPressTriggered = false;
    void Update()
    {
        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
        }
        if (timeLeft <= 0 && IsReloading)
        { 
            AmmoLeft = Ammo;
            IsReloading = false;
            OnReloaded.Invoke();
        } 
        if (Input.GetButtonDown(ButtonName) && !IsDown)
        {
            IsDown = true;
            downPressTriggered = false;
        }
        if ((!Input.GetButton(ButtonName) || (AmmoLeft <= 0)) && IsDown)
        {
            OnRelease.Invoke();
            IsDown = false;
        }
        if(timeLeft <= 0 && AmmoLeft > 0 && IsDown)
        {
            timeLeft = Cooldown;
            AmmoLeft--;
            if (AmmoLeft <= 0 && ReloadTime > 0)
            {
                timeLeft = ReloadTime;
                IsReloading = true;
                OnReload.Invoke();
            }
            if (!downPressTriggered)
            {
                downPressTriggered = true; 
                OnPress.Invoke();
            }
            OnTrigger.Invoke();
        }
        if(!string.IsNullOrEmpty(ReloadButtonName ) && Input.GetButtonDown(ReloadButtonName))
        {
            timeLeft = ReloadTime;
            IsReloading = true;
            OnReload.Invoke();
        }
    }
}
