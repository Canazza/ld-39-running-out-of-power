﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Linq.Expressions;
public interface IResettable
{
    void DoReset();
}
public class LevelController : MonoBehaviour {
    public List<IResettable> Resetables;
    public bool ChildrenOnly;
	// Use this for initialization
	void Start () {
        if (ChildrenOnly)
        {
            Resetables = this.GetComponentsInChildren<IResettable>().ToList();
        }
        else
        {
            Resetables = GameObject.FindGameObjectsWithTag("Resettable").Select(x => x.GetComponent<IResettable>()).ToList();
        }
	}
	public void ResetAll()
    {
        Resetables.ForEach(x => x.DoReset());
    }
}
