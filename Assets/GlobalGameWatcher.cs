﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GlobalGameWatcher : MonoBehaviour, IResettable {

    public string FlagName;
    public UnityEvent PlayerHasItem,PlayerDoesntHaveItem;
    public void Start()
    {
        DoReset();
    }
    public void DoReset()
    {
        if(PlayerInventory.Instance!= null && PlayerInventory.Instance.HasItem(FlagName))
        {
            PlayerHasItem.Invoke();
        } else
        {
            PlayerDoesntHaveItem.Invoke();
        }
    }
}
