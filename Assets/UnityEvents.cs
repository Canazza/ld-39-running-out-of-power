﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace com.clydebuildgames.common
{
    [System.Serializable]
    public class GameObjectEvent : UnityEvent<GameObject> { }
    [System.Serializable]
    public class StringEvent : UnityEvent<string> { }
    [System.Serializable]
    public class BoolEvent : UnityEvent<bool> { }
    [System.Serializable]
    public class IntEvent : UnityEvent<int> { }
    [System.Serializable]
    public class Vector2Event : UnityEvent<Vector2> { }
    [System.Serializable]
    public class Vector3Event : UnityEvent<Vector3> { }
    [System.Serializable]
    public class ColourEvent : UnityEvent<Color> { }
    [System.Serializable]
    public class FloatEvent : UnityEvent<float> { }
}
