﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KelvinTransformReset : MonoBehaviour {
    public Vector3 Position, Scale;
    public Quaternion Rotation;
    private bool stored = false;
    public bool ImmuneToReset = false;
    public UnityEvent OnReset;
    public void Awake()
    {
        Rotation = this.transform.rotation;
        Position = this.transform.position;
        Scale = this.transform.localScale;
        stored = true;
    }
    public void ResetTransform()
    {
        if (!stored) return;
        this.transform.position = Position;
        this.transform.rotation = Rotation;
        this.transform.localScale = Scale;
        if (this.GetComponent<Rigidbody2D>() != null) this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        OnReset.Invoke();
    }

    internal void RememberNewPosition()
    {
        if (ImmuneToReset) return;
        Rotation = this.transform.rotation;
        Position = this.transform.position;
        Scale = this.transform.localScale;
        stored = true;
    }
}
