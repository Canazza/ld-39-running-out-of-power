﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class KelvinScreenTrimmer : MonoBehaviour {
    public KelvinCameraFollow Follower;
    public float Distance = 15;
    public bool doCull;
    public bool DoCull { get; set; }
    [Tooltip("Use Edge distances instead of center differences")]
    public bool FromEdges;
    void Awake()
    {
        DoCull = doCull;
        if (Follower == null) Follower = this.GetComponent<KelvinCameraFollow>();
    }
    public void TrimCurrentScreens()
    {
        if (!DoCull) return;
        var screen = Follower.GetCurrentScreen();
        if (screen == null) return;
        //var withDistances = Follower.Screens.Select(x => new { distance= Vector2.Distance(screen.transform.position, x.transform.position), screen=x });
        Bounds calculationBounds = new Bounds();
        Follower.Screens.ForEach((a) =>
        {
            bool isActive = true;
            if (FromEdges)
            {
                calculationBounds.center = a.PhysicalBounds.center;
                calculationBounds.extents = a.PhysicalBounds.extents;
                calculationBounds.Expand(Distance);
                isActive = calculationBounds.Intersects(screen.PhysicalBounds);
            }
            else
            {
                var d = Vector2.Distance(a.transform.position, screen.transform.position);
                isActive = d <= Distance;
            }
            a.gameObject.SetActive(isActive);
        });

    }
}
