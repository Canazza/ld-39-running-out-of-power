﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
 [System.Serializable]
    public class KelvinScreenTransition
{
        public float TransitionTime = 0.25f;
        public bool FreezeGameOnTransition = true;
        public bool SetPositionOnMidPoint = false;
        [System.Serializable]
        public class floatEvent : UnityEvent<float> { }
        [System.Serializable]
        public class positionEvent : UnityEvent<Vector3> { }
        public UnityEvent OnTransitionStart, OnTransitionMidPoint, OnTransitionEnd;
        public floatEvent OnTransitionUpdate;
        public floatEvent OnTransitionInOutUpdate;
        public positionEvent OnTransitionPositionUpdate;
    public float MaximumTransitionDistance = 30;
        public IEnumerator ScreenTransition(Transform Screen,  Vector3 targetPos)
    {
        if (Vector3.Distance(Screen.position, targetPos) > MaximumTransitionDistance)
        {
            Debug.Log("TRANSITION TOO FAR: " + Vector3.Distance(Screen.position, targetPos));
            Screen.position = targetPos;
            OnTransitionEnd.Invoke();
            yield break;
        }
        var scale = Time.timeScale;
            if (FreezeGameOnTransition)
            {
                Time.timeScale = 0;
            }
            float t = 0;

            var startPos = Screen.position; 
        
            System.DateTime start = System.DateTime.Now;        
            System.DateTime now = System.DateTime.Now;
            OnTransitionStart.Invoke();
        bool midpointReached = false;
            while (t < 1)
            {
                OnTransitionPositionUpdate.Invoke(Vector3.Lerp(startPos, targetPos, t));
                OnTransitionUpdate.Invoke(t);
                OnTransitionInOutUpdate.Invoke(Mathf.Sin((float)(t * Math.PI)));
                yield return null;
                now = System.DateTime.Now;
                t = ((float)(now - start).TotalSeconds) / TransitionTime;
                if (t >= 0.5 && !midpointReached)
                {
                    midpointReached = true;
                    OnTransitionMidPoint.Invoke();
                    if (SetPositionOnMidPoint)
                        Screen.position = targetPos;
                }
                //Screen.position = Vector3.Lerp(startPos, targetPos, t);
            }
            Screen.position = targetPos;
            OnTransitionEnd.Invoke();
            Time.timeScale = scale;
        
        }
    } 
