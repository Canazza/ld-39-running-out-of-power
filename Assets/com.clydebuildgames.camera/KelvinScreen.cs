﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;
using System;

public class KelvinScreen : MonoBehaviour { 
    private bool isDirty = true;
    public bool IsHiddenRoom = false;
    public bool IsDiscovered = false;
    public bool ConnectionUp, ConnectionDown, ConnectionLeft, ConnectionRight;
    public Dictionary<string, bool> ChildActivationStates = new Dictionary<string, bool>();
    public void SetState(string state, bool isOn)
    {
        if (!ChildActivationStates.ContainsKey(state)) ChildActivationStates.Add(state, isOn);
        else ChildActivationStates[state] = isOn;
    }
    public void Clear()
    {
        isDirty = true;
    }
    private Bounds _physicalBounds;
    public Bounds PhysicalBounds
    {
        get
        { 
            return _physicalBounds;
        }
    }
    public Bounds Bounds;
    private Bounds _cameraBounds;
    public Bounds CameraBounds
    {
        get
        { 
            return _cameraBounds;
        }
    }
    public Vector3 Offset;
    public float OrthoScale = 1;
    public UnityEvent OnActivated, OnDeactivated, OnLeave;
    public KelvinScreenGroup Group;
    public void OnScreenActivated()
    {
        foreach(var reset in this.GetComponentsInChildren<KelvinTransformReset>())
        {
            reset.ResetTransform();
        }
        OnActivated.Invoke();
        if (Group != null) Group.AnyActivated();

    }
    public void OnScreenDeactivated()
    {
        foreach (var reset in this.GetComponentsInChildren<KelvinTransformReset>())
        {
            reset.ResetTransform();
        }
        OnDeactivated.Invoke();
        if (Group != null) Group.AnyDeactivated();
    }
    
    public void OnLeaveScreen()
    {
        OnLeave.Invoke();
    }
    private void Cleanup()
    { 
            _physicalBounds = new Bounds(Bounds.center + this.transform.position,
            Bounds.size);
        _cameraBounds = new Bounds(PhysicalBounds.center, new Vector3(
            Mathf.Max(0, PhysicalBounds.size.x - (Camera.main.orthographicSize * Camera.main.aspect) * 2 * Camera.main.rect.width),
            Mathf.Max(0, PhysicalBounds.size.y - (Camera.main.orthographicSize) * 2 * Camera.main.rect.height),
            0

            ));
        isDirty = false; 
    }
        
	// Use this for initialization
	void Awake () {
        Cleanup();
        OnScreenDeactivated();
        Group = this.gameObject.GetComponentInParent<KelvinScreenGroup>();
	}
	
	// Update is called once per frame
	void Update () {
        if (isDirty) Cleanup();
	}
    void OnDrawGizmos()
    {
        if(!Application.isPlaying) Cleanup();
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(PhysicalBounds.center, PhysicalBounds.size);
        Gizmos.color = Color.white;
        Gizmos.DrawWireCube(CameraBounds.center, CameraBounds.size);
        
    }
}
