﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class KelvinMiniMap : MonoBehaviour {
    public KelvinCameraFollow follower;

    public Sprite Undiscovered, Discovered, Current;
    public Color UndiscoveredColour, DiscoveredColour, CurrentColour;
    public float Scale = 1;
    public List<GameObject> Pool;
    public List<GameObject> connectorPool;
    public GameObject prefab;
    public bool FollowCurrentScreen = true;
    
    private GameObject GetFromPool()
    {
        var existing = Pool.FirstOrDefault(x => x.activeSelf == false);
        if(existing == null)
        {
            existing = Instantiate(prefab);
            existing.transform.SetParent(this.transform, false);
            Pool.Add(existing);
        }
        existing.SetActive(true);
        return existing;
    }
    [ContextMenu("UpdateMap")]
    void UpdateMap()
    {
        var currentScreen = follower.GetCurrentScreen();
        Pool.ForEach(x => x.SetActive(false));
        foreach(var screen in follower.Screens)
        {
            if (screen.IsHiddenRoom && !screen.IsDiscovered) continue;
            var mapBit = GetFromPool();
            mapBit.name = screen.name;
            var rt = mapBit.GetComponent<RectTransform>();
            rt.anchoredPosition = new Vector2((screen.transform.position.x  ) + screen.Bounds.center.x, (screen.transform.position.y) + screen.Bounds.center.y) * Scale;
            rt.sizeDelta = new Vector2(screen.Bounds.size.x - 2, screen.Bounds.size.y -2) * Scale;
            var sprite = mapBit.GetComponent<Image>();
            if (sprite == null)
                sprite = mapBit.GetComponentsInChildren<Image>().FirstOrDefault(x => x.name == "Main");
            if (sprite == null)
                sprite = mapBit.GetComponentInChildren<Image>();
            if (currentScreen == screen)
            {
                sprite.sprite = Current;
                sprite.color = CurrentColour;

            }
            else if (screen.IsDiscovered)
            {
                sprite.sprite = Discovered;
                sprite.color = DiscoveredColour;
                
            }
            else
            {
                sprite.sprite = Undiscovered;
                sprite.color = UndiscoveredColour;
            }
            var border = mapBit.GetComponentsInChildren<Image>().FirstOrDefault(x => x.name == "Border");
            if (border != null)
            {
                border.color = new Color(border.color.r, border.color.g, border.color.b, sprite.color.a);
            }
            UpdateConnectors(screen, mapBit.gameObject);
        }
        if (currentScreen != null && FollowCurrentScreen)
        {
            this.GetComponent<RectTransform>().anchoredPosition = - new Vector2((currentScreen.transform.position.x ) + currentScreen.Bounds.center.x, (currentScreen.transform.position.y ) + currentScreen.Bounds.center.y) * Scale;
        } else
        {
            worldBounds = follower.GetWorldBounds();
            this.GetComponent<RectTransform>().anchoredPosition = -(Vector2.one * 5 * Scale) - worldBounds.center * Scale;
        }

    }
    public Rect worldBounds;
    void UpdateConnectors(KelvinScreen screen, GameObject sprite)
    {
        sprite.transform.Find("Left").gameObject.SetActive(screen.IsDiscovered && screen.ConnectionLeft);
        sprite.transform.Find("Right").gameObject.SetActive(screen.IsDiscovered && screen.ConnectionRight);
        sprite.transform.Find("Top").gameObject.SetActive(screen.IsDiscovered && screen.ConnectionUp);
        sprite.transform.Find("Bottom").gameObject.SetActive(screen.IsDiscovered && screen.ConnectionDown);
        foreach (var item in screen.ChildActivationStates)
        {
            var itemTransform = sprite.transform.Find(item.Key);
            if (itemTransform == null) continue;
            itemTransform.gameObject.SetActive(item.Value);
        }
       
    }
    void Start()
    {
        Pool.ForEach(x => Destroy(x.gameObject));
        Pool = new List<GameObject>();
        follower.OnScreenChange.AddListener(() => UpdateMap());
    }
}
