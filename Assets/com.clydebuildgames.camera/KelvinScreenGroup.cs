﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KelvinScreenGroup : MonoBehaviour {
    public AudioClip GroupMusic;
    public AudioSource GroupMusicSource;
    public UnityEvent OnAnyActivate, OnAnyDeactivate;
    [Multiline]
    public string AreaName;
    public void AnyActivated()
    {
        if(GroupMusicSource != null && GroupMusicSource.clip != GroupMusic)
        {
            GroupMusicSource.clip = GroupMusic;
            GroupMusicSource.Play();
        }
        OnAnyActivate.Invoke();
    }
    public void AnyDeactivated()
    {
        OnAnyDeactivate.Invoke();
    }
}
