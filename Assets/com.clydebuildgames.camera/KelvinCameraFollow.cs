﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class KelvinCameraFollow : MonoBehaviour { 
    public Transform Player;
    public KelvinScreenTransition DefaultTransition;
    public List<KelvinScreen> Screens;
    private KelvinScreen lastScreen = null;
    public Vector2 ShakeOffset;
    public bool FastLerp = false;
    public void Shake(float duration)
    {
        Shake(duration, 0.075f);
    }
    public void Shake(float duration, float violence)
    {
        StartCoroutine(DoShake(duration, violence));
    }
    private IEnumerator DoShake(float duration, float violence)
    {
        for(float t = 0; t < duration; t+=Time.deltaTime)
        {
            ShakeOffset = (Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360)) * Vector2.right) * violence;
            yield return null;
        }
        ShakeOffset = Vector2.zero;
    }
    public Vector3 PlayerPosition
    {
        get
        {
            return Player.position + (lastScreen != null? lastScreen.Offset:Vector3.zero);
        }
    }
    public void LoadScene(int scene)
    {
        SceneManager.LoadScene(scene);
    }
    public void LoadSceneByName(string name)
    {
        SceneManager.LoadScene(name);
    }
    public UnityEvent OnScreenChange;
    void Awake()
    {
        LookForScreens();
    }
    [ContextMenu("Add all active screens")]
    public void LookForScreens()
    {
        var found = GameObject.FindObjectsOfType<KelvinScreen>();
        foreach(var screen in found) {
            if(Screens.Contains(screen)) continue;
            Screens.Add(screen);
            screen.Clear();
        }
        Screens.RemoveAll(x => x == null);
        lastScreen = null;
    }
    public KelvinScreen GetCurrentScreen()
    {
        if (Screens == null || Screens.Count == 0) return null;
        foreach (var screen in Screens)
        {
            if (screen == null) continue;
            if (screen.PhysicalBounds.Contains(Player.position)) return screen;
        }
        return lastScreen;//  ?? Screens[0];
    }
    // Update is called once per frame
    void Update() {
        SetPos();
    }
    void LateUpdate()
    {
        SetPos();
    }
    void SetPos() { 
        if(Time.timeScale <= 0) return;
        if (Screens.Count == 0) return;
        var currentScreen = GetCurrentScreen(); 
        if (currentScreen == null) return;
        var targetPosition = currentScreen.CameraBounds.ClosestPoint((Vector2)PlayerPosition) - this.transform.forward;
        if (lastScreen != currentScreen )
        {
            StopAllCoroutines();
            ShakeOffset = Vector2.zero;
            currentScreen.IsDiscovered = true;
            currentScreen.OnScreenActivated();
            if (lastScreen != null)
            { 
                lastScreen.OnScreenDeactivated();
                lastScreen.OnLeaveScreen();
                KelvinTransitionOverride transitionOverride = currentScreen.GetComponent<KelvinTransitionOverride>();
                
                var transition = transitionOverride != null? transitionOverride.transition: DefaultTransition; 
                StartCoroutine(transition.ScreenTransition(this.transform,targetPosition));
                if(lastScreen.OrthoScale != currentScreen.OrthoScale)
                {
                    StartCoroutine(TweenOrthoScale(lastScreen.OrthoScale, currentScreen.OrthoScale,transition.TransitionTime));
                }
            }
            lastScreen = currentScreen;
            OnScreenChange.Invoke();
        } else 
        if (currentScreen != null)
        {
            if (FastLerp && Vector3.Distance(this.transform.position, targetPosition) > 0.3f)
            {
                this.transform.position = Vector3.Lerp(this.transform.position, targetPosition,0.3f) + (Vector3)ShakeOffset;
            }
            else
            {
                this.transform.position = targetPosition + (Vector3)ShakeOffset;
            }
            lastScreen = currentScreen;
        }
            
	}

    internal Rect GetWorldBounds()
    {
        Rect r = new Rect();
        foreach(var item in Screens)
        {
            r.xMin = Mathf.Min(r.xMin, item.transform.position.x);
            r.xMax = Mathf.Max(r.xMax, item.transform.position.x);
            r.yMin = Mathf.Min(r.yMin, item.transform.position.y);
            r.yMax = Mathf.Max(r.yMax, item.transform.position.y);
        }
        return r;
        
    }
    private float originalOrthoScale = -1;
    private IEnumerator TweenOrthoScale(float orthoScale1, float orthoScale2,float time)
    {
        var cam = this.GetComponent<Camera>();
        if (originalOrthoScale < 0) originalOrthoScale = cam.orthographicSize;
        for (float t = 0; t < 1; t += Time.unscaledDeltaTime / time)
        {
            cam.orthographicSize = Mathf.Lerp(originalOrthoScale * orthoScale1, originalOrthoScale * orthoScale2, t);
            yield return null;
        }
        cam.orthographicSize = originalOrthoScale * orthoScale2;
    } 
}
