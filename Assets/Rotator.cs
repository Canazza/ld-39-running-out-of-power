﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour, IResettable {
    public float RotationSpeed = 2;
    public float Angle = 0;
    private float _angle;
    public bool RotateChildren = false;
    private bool reversed = false;
    public void Reverse()
    {
        reversed = !reversed;
    }
    void Update()
    {
        _angle += RotationSpeed * Time.deltaTime * (reversed ? -1 : 1);
        while (_angle < 0) _angle += 360;
        _angle %= 360;
        this.transform.rotation = Quaternion.Euler(0, 0, _angle);
        if (!RotateChildren)
        {
            for (var i = 0; i < this.transform.childCount; i++)
            {
                this.transform.GetChild(i).rotation = Quaternion.identity;
            }
        }
    }
    void Start()
    {
        _angle = Angle;
    }
    public void DoReset()
    {
        reversed = false;
        _angle = Angle;
    }
}
