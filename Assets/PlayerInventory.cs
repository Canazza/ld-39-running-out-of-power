﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using UnityEngine;

public class PlayerInventory : MonoBehaviour {
    public static PlayerInventory Instance;
    public List<string> ItemsInInventory;
    public void AddToInventory(string itemName)
    {
        ItemsInInventory.Add(itemName);
        FindObjectsOfType<GlobalGameWatcher>().ToList().ForEach(x => x.DoReset());
    }
	// Use this for initialization
	void Start () {
        if (Instance != null && Instance != this) throw new System.Exception("Instance already exists");
        Instance = this;
	} 
    public bool HasItem(string name)
    {
        return ItemsInInventory.Any(n => n == name);
    }
}
