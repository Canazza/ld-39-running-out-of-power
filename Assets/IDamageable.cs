﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface IDamageable
{
    void TakeDamage(int damageToTake, params string[] damageTypes);
    void HealDamage(int damageToHeal, params string[] damageTypes);

}