﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace com.clydebuiltgames.platformer.Data
{
    [System.Serializable]   
    public class DetectorGroup
    {
        public static DetectorGroup Default
        {
            get
            {
                return new DetectorGroup()
                {
                    GroupName = "Default",
                    Up = Detector.CreateDefault(90, .5f, 1, Color.red),
                    Down = Detector.CreateDefault(-90, 0.5f, 1, Color.green),
                    Left = Detector.CreateDefault(180, 1.5f, 0.5f, Color.blue),
                    Right = Detector.CreateDefault(0, 1.5f, 0.5f, Color.yellow)
                };
            }
        }
        public void DrawGizmo(Vector2 from, Quaternion rotation)
        { 
            Up.DrawGizmo(from,rotation); 
            Down.DrawGizmo(from, rotation); 
            Left.DrawGizmo(from, rotation); 
            Right.DrawGizmo(from, rotation);
        }
        public string GroupName;
        public Detector Up, Down, Left, Right;
    }
    [System.Serializable]
    public class Detector
    {
        public float Angle;
        public Vector2 Size;
        public Vector2 Offset;


        public float Distance;
        public float TotalDistance
        {
            get
            {
                return Distance - Offset.x;
            }
        }
        public LayerMask Mask;
        public Color DebugColour;
        public Vector2 Direction
        {
            get
            {
                return Quaternion.AngleAxis(Angle, Vector3.back) * Vector2.left;
            }
        }
        public Vector2 OffsetRotated
        {
            get
            {
                return Quaternion.AngleAxis(Angle, Vector3.back) * Offset;
            }
        }
         

        public Matrix4x4 RealignMatrix(Vector2 from, Quaternion rotation) {
            return Matrix4x4.TRS(from, CalculateRotation(rotation), Vector3.one);
            
        }

        Quaternion CalculateRotation(Quaternion rot)
        {
            return Quaternion.AngleAxis(Angle - rot.eulerAngles.z, Vector3.back);
        }
        public Vector2 CalculateRotatedVector2(Vector2 v, Quaternion rotation)
        { 
            return CalculateRotation(rotation) * v;
        }
        public RaycastHit2D RayCast(Vector2 from, Quaternion rotation)
        {
            return Physics2D.Raycast(from + CalculateRotatedVector2(Offset, rotation), CalculateRotation(rotation) * Vector2.left, Distance, Mask);
        }
        public RaycastHit2D Cast(Vector2 from,Quaternion rotation)
        {
            return Physics2D.BoxCast(from + CalculateRotatedVector2(Offset, rotation), Size, rotation.eulerAngles.z - Angle, CalculateRotation(rotation)* Vector2.left, Distance, Mask);
        }
        public RaycastHit2D[] CastAll(Vector2 from, Quaternion rotation)
        {
            //Debug.DrawLine(from + CalculateRotatedVector2(Offset, rotation), from + CalculateRotatedVector2(Offset, rotation) + (Vector2)(CalculateRotation(rotation) * Vector2.left) * Distance, DebugColour, 1);
            return Physics2D.BoxCastAll(from + CalculateRotatedVector2(Offset, rotation), Size, rotation.eulerAngles.z - Angle, CalculateRotation(rotation) * Vector2.left, Distance, Mask);
        }
        public void DrawGizmo(Vector2 from, Quaternion rotation)
        {
            var om = Gizmos.matrix;
            Gizmos.matrix = Matrix4x4.TRS(from, CalculateRotation(rotation), Vector3.one);
            Gizmos.color = DebugColour;
            Gizmos.DrawLine(Offset, Offset + Vector2.left * Distance); 
            Gizmos.DrawWireCube(Offset + Vector2.left * Distance, Size);
            Gizmos.DrawWireCube(Offset, Size); 
            Gizmos.matrix = om;
        }

        public static Detector CreateDefault(float Angle, float Size = 1, float Distance = 1, Color? debugColour = null)
        {

            Detector model = new Detector();
            model.DebugColour = debugColour ?? Color.magenta;
            model.Angle = Angle;
            model.Mask = (LayerMask)(-1);
            model.Offset = Vector2.zero;
            model.Size = new Vector2(0.01f, Size);
            model.Distance = Distance;
            return model;
        }
    }
}
