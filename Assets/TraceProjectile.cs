﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraceProjectile : MonoBehaviour, IVelocityInherit
{
    public float HitSize = 0.2f;
    public float Speed = 10;
    public Vector2 Inherited;
    public float Lifetime = 1;
    public int Damage = 1;
    private float LifeLeft;
    public LayerMask CollidableMask;
    void OnEnable()
    {
        LifeLeft = Lifetime;
    }
	// Update is called once per frame
    void Update()
    {
        LifeLeft -= Time.deltaTime;
        if (LifeLeft < 0) this.gameObject.SetActive(false);
    }
	void FixedUpdate () {
        this.transform.Translate((Vector2.right * (Speed+ Inherited.magnitude)) * Time.fixedDeltaTime, Space.Self);
        var hit = Physics2D.OverlapCircle(this.transform.position, HitSize, CollidableMask);
        if(hit != null)
        {
            LifeLeft = 0;
            IDamageable damagable = hit.GetComponent<IDamageable>();
            Debug.Log("HIT " + damagable,hit);
            if (damagable != null) damagable.TakeDamage(Damage);
        }
	}
    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(this.transform.position, HitSize);
    }
    public void Inherit(Vector2 velocity)
    {
        Inherited = velocity;
    }
}
