﻿using com.clydebuildgames.common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColourByHeight : MonoBehaviour {
    public Gradient gradient;
    public ColourEvent OnColour;
    public float MinY, MaxY;
	// Update is called once per frame
	void Update () {
        float t = (this.transform.position.y - MinY) / (MaxY - MinY);
        OnColour.Invoke(gradient.Evaluate(t));
	}
}
