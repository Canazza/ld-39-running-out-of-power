﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationCycle : MonoBehaviour,IResettable {
    public Animator animator;
    public string AnimatorBool = "FireOn";
    public UnityEvent CycleOn, CycleOff;
    public float CycleTime = 2;
    public AnimationCurve Cycle;
    public float OnThreshold = 1;
    public float OffThreshold = 0;
    public bool IsOn;
    public float t = 0;
    public float value;
    private float initialT = 0;
	// Use this for initialization
	void Start () {
        initialT = t;
        if (animator == null) animator = this.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        t += Time.deltaTime / CycleTime;
        t %= Cycle.keys[Cycle.keys.Length - 1].time;
        value = Cycle.Evaluate(t);
        if(IsOn && value <= OffThreshold)
        {
            IsOn = false;
            CycleOff.Invoke();
            if (animator != null) animator.SetBool(AnimatorBool, IsOn);
        } else if(!IsOn && value >= OnThreshold)
        {
            IsOn = true;
            CycleOn.Invoke();
            if (animator != null) animator.SetBool(AnimatorBool, IsOn);
        }

	}

    public void DoReset()
    {
        t = initialT;
    }
}
