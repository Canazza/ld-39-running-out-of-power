﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class StateTrigger : MonoBehaviour, ITriggerable, IResettable {
    public string[] OnlyTags;
    public UnityEvent OnTrigger,OnReset;
    public bool OnceOnly;
    public bool IsResettable = false;
    public void Trigger(GameObject source)
    {
        if (!this.enabled) return;
        if (OnlyTags.Length > 0 && !OnlyTags.Contains(source.tag)) return;
        OnTrigger.Invoke();
        if (OnceOnly) this.enabled = false;
    }

    public void DoReset()
    {
        if (IsResettable)
        {
            this.enabled = true;
            OnReset.Invoke();
        }
    }
}
